*** Settings ***
Resource    resources.robot

*** Variables ***
${BROWSER}  ff

*** Test Cases ***
G001 Búsqueda básica en Google desde Firefox
    Open Browser And Check Logo
    Input Text   xpath=//*[@id="tsf"]/div[2]/div[1]/div[1]/div/div[2]/input   ${ToSearch}
    Click Element   xpath=//*[@id="hplogo"]
    Click Element   xpath=//*[@id="tsf"]/div[2]/div[1]/div[3]/center/input[1]
    Title Should Be   ${ToSearch} - Buscar con Google 
    Page Should Contain   ${ToSearch}
    Close Browser

G002 Búsqueda vácia en Google desde Firefox
    Open Browser And Check Logo
    Click Element   xpath=//*[@id="tsf"]/div[2]/div[1]/div[3]/center/input[1] 
    Title Should Be   Google
    Close Browser