*** Settings ***
Library   SeleniumLibrary
Resource   TagsResources.robot

***Test Cases***
C0001 Testing URL
    [Tags]   TestingTest   
    Open URL
    Title Should Be   Hola Mundo!
    Set Focus To Element   xpath=/html/body/div[1]/div/div[2]/a[1]
    Click Element   xpath=/html/body/div[1]/div/div[2]/a[1]
    Wait Until Element Is Visible   xpath=//*[@id="page-header"]/div[1]/div/div/div/a
    Title Should Be   Winston Castillo – Un sitio para comunicarse
    Close Browser

C0001 popup windows
    [Tags]   TestingTest2
    Open URL
    Title Should Be   Hola Mundo!
    Set Focus To Element   xpath=/html/body/div[1]/div/div[2]/a[2]
    Click Link   xpath=/html/body/div[1]/div/div[2]/a[2]
    Title Should Be   Hola Mundo!
    Wait Until Element Is Visible   xpath=//*[@id="exampleModal"]/div/div/div[3]/button[1]
    Close Browser