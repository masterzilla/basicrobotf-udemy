*** Settings ***
Library   SeleniumLibrary
Suite Teardown  Close All Browsers

*** Variables ***
${BROWSER}   chrome
${URL}   http://automationpractice.com/index.php

***Test Cases***
CP0001 Probar la función de previsualización en todos los objetos del homepage
    Open Browser   ${URL}   ${BROWSER}
    Maximize Browser Window
    Wait Until Element Is Visible   xpath=//*[@id="header_logo"]/a/img
    Set Focus To Element   xpath=//*[@id="home-page-tabs"]/li[1]/a
    Element Should Not Be Visible   xpath=//*[@id="homefeatured"]/li[4]/div/div[1]/div/div[1]/a
    Get Selenium Implicit Wait
    Mouse Over   xpath=//*[@id="homefeatured"]/li[1]/div/div[1]/div
    Click Element   xpath=//*[@id="homefeatured"]/li[1]/div/div[1]/div/a[2]/span
    Set Focus To Element   xpath=//*[@id="homefeatured"]/li[1]/div/div[1]/div/a[2]/span
    Title Should Be    My Store
    Select Frame   xpath=//iframe[@class="fancybox-iframe"]
    sleep  1s
    Wait until element is visible  xpath=//*[@id="product"]/div/div/div[2]/p[7]/button[1]  timeout=10s
    Click Element  xpath=//*[@id="product"]/div/div/div[2]/p[7]/button[1]
    Unselect frame
    sleep   1s
    Click Element   xpath=//a[@title="Close"]
    sleep   1s
    Close Browser
