*** Settings ***
Library   SeleniumLibrary

*** Variables ***
${BROWSER}   chrome
${URL}   http://automationpractice.com/index.php

***Test Cases***
CP0001 Probar la función de previsualización en todos los objetos del homepage
    Open Browser   ${URL}   ${BROWSER}
    Maximize Browser Window
    Wait Until Element Is Visible   xpath=//*[@id="header_logo"]/a/img
    Set Focus To Element   xpath=//*[@id="home-page-tabs"]/li[1]/a
    Element Should Not Be Visible   xpath=//*[@id="homefeatured"]/li[4]/div/div[1]/div/div[1]/a
    Get Selenium Implicit Wait
    Mouse Over   xpath=//*[@id="homefeatured"]/li[1]/div/div[1]/div
    Click Element   xpath=//*[@id="homefeatured"]/li[1]/div/div[1]/div/a[2]/span
    Set Focus To Element   xpath=//*[@id="homefeatured"]/li[1]/div/div[1]/div/a[2]/span
    Title Should Be    My Store
    Select Frame   xpath=//*[@id="fancybox-frame1579880958698"]
    Close Browser
