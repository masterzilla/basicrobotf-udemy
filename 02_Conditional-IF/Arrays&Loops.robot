*** Settings ***
Library   SeleniumLibrary
Library   String

*** Variables ***
${BROWSER}  chrome
${URL}  automationpractice.com/index.php
${SCHEME}   http
${TestURL}   ${SCHEME}://${URL}

*** Keywords ***
Open URL
    Open Browser   ${TestURL}   ${BROWSER}

*** Test Cases ***
C0001 Checking group of containers
    Open URL
    Set Global Variable   @{ContainersName}   //*[@id="homefeatured"]/li[1]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[2]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[3]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[4]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[5]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[6]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[7]/div/div[2]/h5/a
    :FOR   ${ContainerName}   IN   @{ContainersName}
    \   Click Element   xpath=${ContainerName}
    \   Wait Until Element Is Visible   xpath=//*[@id="bigpic"]
    \   Click Element   xpath=//*[@id="header_logo"]/a/img
    Close Browser