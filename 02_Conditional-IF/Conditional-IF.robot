*** Settings ***
Library   SeleniumLibrary

*** Variables ***
${BROWSER}  chrome
${URL}  http://automationpractice.com/index.php
${OptionScreen}   Other

*** Keywords ***
Select Women Option
    Click Element   xpath=//*[@id="block_top_menu"]/ul/li[1]/a
    Title Should Be   Women - My Store

Select Dresses Option
    Click Element   xpath=//*[@id="block_top_menu"]/ul/li[1]/a
    Title Should Be   Dresses - My Store

*** Test Cases ***
0001 Conditional practice
    Open Browser   ${URL}   ${BROWSER}
    Wait Until Element Is Visible   xpath=//*[@id="header_logo"]/a/img
    Run Keyword If   '${OptionScreen}'=='Women'   Select Women Option   Else   Select Dresses Option
    Close Browser